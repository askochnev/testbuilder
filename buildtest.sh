#!/bin/bash

unitySearchLocation="C:/Program Files/Unity"
unityPath=$(./Utils/UnityPathLocator/UnityPathLocator/bin/Release/UnityPathLocator.exe "$unitySearchLocation")
#echo $unityPath
unityPath=${unityPath//\\/\/} 
#echo $unityPath
echo "$unityPath -quit -batchmode -logFile Builds/build.log -executeMethod AppBuilder.BuildGameTest"
eval "$unityPath -projectPath ./HelloWorld -quit -batchmode -logFile Builds/build.log -executeMethod AppBuilder.BuildGameTest"