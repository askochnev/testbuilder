﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VersionLabel : MonoBehaviour
{
    [SerializeField]
    public Text _text;
    // Start is called before the first frame update
    void Start()
    {
        _text.text = Application.version;
    }

}
