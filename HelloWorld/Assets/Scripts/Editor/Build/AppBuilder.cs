﻿using System.Diagnostics;
using UnityEditor;
using UnityEngine;

public class AppBuilder
{
    [MenuItem("Build/Android Test")]
    public static void BuildGameTest()
    {
        BuildGame(BuildType.Test);
    }

    [MenuItem("Build/Andropid Production")]
    public static void BuildGameProduction()
    {
        BuildGame(BuildType.Production);
    }

    public static void BuildGame(BuildType buildType)
    {
        IncrementAndGetVersion(buildType);
        UnityEngine.Debug.Log(GetDataPath(buildType));
        BuildPipeline.BuildPlayer(GetBuildPlayerOptions(buildType));
    }

    private static string GetDataPath(BuildType buildType)
    {
        var appName = GetAppName(buildType);
        return Application.dataPath.Replace("Assets", "") + "../Builds/" + appName + "/" + appName + GetAppFilenameExtension();
    }
    
    private static string GetAppFilenameExtension()
    {
        return ".apk";
    }

    private static string GetAppName(BuildType buildType)
    {
        switch (buildType)
        {
            case BuildType.Test:
                return "Hello_World(test)";
        }
        return "Hello_World";
    }

    private static BuildPlayerOptions GetBuildPlayerOptions(BuildType buildType)
    {
        var opt = new BuildPlayerOptions();

        string path = GetDataPath(buildType);
        string[] levels = new string[] { "Assets/Scenes/SampleScene.unity" };
        opt.locationPathName = path;
        opt.scenes = levels;
        opt.target = BuildTarget.Android;
        PlayerSettings.Android.useAPKExpansionFiles = (buildType == BuildType.Production);
        opt.options = BuildOptions.None;
        return opt;
    }

    private static void IncrementAndGetVersion(BuildType buildType)
    {
        var buildNumber = EditorPrefs.GetInt(buildType.ToString() + "Version");
        buildNumber++;

        PlayerSettings.bundleVersion = "Build " + buildNumber + "(" + buildType.ToString() + ")";

        EditorPrefs.SetInt(buildType.ToString() + "Version", buildNumber);
    }

    public enum BuildType
    {
        Test = 1,
        Production = 2
    }
}
