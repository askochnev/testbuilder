﻿using System.IO;
using System.Collections.Generic;
using System.Diagnostics;

namespace UnityPathLocator
{
    class UnityLocator
    {
        private List<FileInfo> unityVersions = new List<FileInfo>();

        public FileInfo LocateNewestUnityVersion(string parentPath)
        {
            unityVersions.Clear();
            var directoryInfo = new DirectoryInfo(parentPath);
            LocateUnityRecursive(directoryInfo);
            if (unityVersions.Count == 0)
            {
                return null;
            }
            var maxUnity = unityVersions[0];
            var maxVersion = FileVersionInfo.GetVersionInfo(maxUnity.FullName).FileVersion;
            foreach (var unity in unityVersions)
            {
                var version = FileVersionInfo.GetVersionInfo(unity.FullName).FileVersion;
                if (string.Compare(version, maxVersion) > 0)
                {
                    maxVersion = version;
                    maxUnity = unity;
                }
            }
            return maxUnity;
        }

        private void LocateUnityRecursive(DirectoryInfo path)
        {
            foreach (var file in path.GetFiles())
            {
                if (file.Name == "Unity.exe")
                {
                    unityVersions.Add(file);
                    return;
                }
            }
            foreach (var dir in path.GetDirectories())
            {
                LocateUnityRecursive(dir);
            }
        }
    }
}
