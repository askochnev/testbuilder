﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnityPathLocator
{
    class Program
    {
        static List<FileInfo> unityVersions = new List<FileInfo>();
        static void Main(string[] args)
        {
            var defaultUnityParentPath = args.Length > 0 ? args[0] : "C:\\Program Files\\Unity";
            var unityLocator = new UnityLocator();
            Console.Write("\"" + unityLocator.LocateNewestUnityVersion(defaultUnityParentPath).FullName + "\"");
        }
    }
}
