echo off
set unityPath="C:\Program Files\Unity"

Utils\UnityPathLocator\UnityPathLocator\bin\Release\UnityPathLocator.exe %unityPath% > build_command.cmd 
echo  -quit -batchmode -projectPath ./HelloWorld -logFile Builds/build.log -executeMethod AppBuilder.BuildGameProduction >> build_command.cmd

call build_command.cmd
del build_command.cmd

echo finished
pause